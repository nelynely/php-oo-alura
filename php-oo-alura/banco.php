<?php

require_once 'autoload.php';

use Alura\Banco\Modelo\Conta\Titular;
use Alura\Banco\Modelo\Endereco;
use Alura\Banco\Modelo\CPF;
use Alura\Banco\Modelo\Conta\Conta;

$endereco = new Endereco('Petrópolis', 'Um bairro', 'minha rua', '71b');
$primeiraConta = new Conta(new Titular(new CPF('609.814.220-39'), 'nelynely', $endereco));
$primeiraConta->deposita(300);
$primeiraConta->saca(50);

echo $primeiraConta->recuperaNomeTitular() . PHP_EOL;
echo $primeiraConta->recuperaCpfTitular() . PHP_EOL;
echo $primeiraConta->recuperaSaldo() . PHP_EOL;

$segundaConta = new Conta(new Titular(new CPF('214.410.970-23'),'james gosling', $endereco));
echo $segundaConta->recuperaNomeTitular() . PHP_EOL;
echo $segundaConta->recuperaCpfTitular() . PHP_EOL;


echo Conta::recuperaNumeroContas();