<?php

use Alura\Banco\Modelo\Endereco;

require_once 'autoload.php';

$umEndereco = new Endereco('Petrópolis', 'bairro qualquer', 'minha rua', '72a');
$outroEndereco = new Endereco('Rio de Janeiro', 'Botafogo', 'Voluntários da Pátria', '190');

//Implementando __get
echo $umEndereco->cidade . PHP_EOL;
//Implementando __set
echo $umEndereco->logradouro = 'beco';

exit();
echo $umEndereco . PHP_EOL;
echo $outroEndereco;