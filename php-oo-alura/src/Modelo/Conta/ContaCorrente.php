<?php


namespace Alura\Banco\Modelo\Conta;


class ContaCorrente extends Conta
{
    protected function percentualTarifa(): float
    {
        return 0.05;
    }
    public function transfere(float $valor, Conta $conta)
    {
        if ($valor > $this->saldo) {
            echo "Saldo indisponível: ";
            return;
        }

        $this->sacar($valor);
        $conta->depositar($valor);

    }
}