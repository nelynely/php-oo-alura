<?php

namespace Alura\Banco\Modelo\Conta;

abstract class Conta
{
    private Titular $titular;
    protected float $saldo;
    private static $numeroContas = 0;

    public function __construct(Titular $titular)
    {
        $this->titular = $titular;
        $this->saldo = 0;

        Conta::$numeroContas++;
    }

    public function __destruct()
    {
        self::$numeroContas--;
    }

    public function recuperaSaldo(): float
    {
        return $this->saldo;
    }


    public function saca(float $valor): void
    {
        $tarifa_saque = $valor * $this->percentualTarifa();
        $valor_saque = $valor + $tarifa_saque;
        if ($valor_saque > $this->saldo) {
            echo "Saldo indisponível: ";
            return;
        }

        $this->saldo -= $valor_saque;

    }

    public function deposita(float $valor): void
    {
        if ($valor < 0) {
            echo "Valor precisa ser positivo: ";
            return;
        }

        $this->saldo = +$valor;
    }

    public static function recuperaNumeroContas(): int
    {
        return self::$numeroContas;
        //return Conta::$numeroContas;
    }

    public function recuperaNomeTitular(): string
    {
        return $this->titular->recuperaNome();
    }

    public function recuperaCpfTitular(): string
    {
        return $this->titular->recuperaCpf();
    }

    public static function recuperaNumeroDeContas(): int
    {
        return self::$numeroDeContas;
    }

    protected abstract function percentualTarifa(): float;

}