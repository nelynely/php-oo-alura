<?php

use Alura\Banco\Modelo\CPF;
use Alura\Banco\Modelo\Funcionario\Diretor;
use Alura\Banco\Service\Autenticador;

require_once 'autoload.php';

$autenticador = new Autenticador();
$umDiretor = new Diretor(
    'Rasmus Lerdorf',
    new CPF('943.589.900-53'),
    10000);

$autenticador->tentaLogin($umDiretor, '3030');