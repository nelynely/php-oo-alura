<?php

require_once 'autoload.php';

use Alura\Banco\Service\ControladorDeBonificacoes;
use Alura\Banco\Modelo\{CPF};
use Alura\Banco\Modelo\Funcionario\{Funcionario, Gerente, Diretor, Desenvolvedor, EditorVideo};

$umFuncionario = new Desenvolvedor('Vinicius Dias',
    new CPF('123.456.789-10'),
    1000);

$umFuncionario->sobeDeNivel();

$umaFuncionario = new Gerente('Patricia',
    new CPF('987.654.321-10'),
    3000);
$umDiretor = new Diretor('Natália',
    new CPF('773.214.352-37'),
    5000);
$umEditor = new EditorVideo('Paulo',
    new CPF('013.420.050-07'),
    1500);

$controlador = new ControladorDeBonificacoes();
$controlador->adicionaBonificaoes($umaFuncionario);
$controlador->adicionaBonificaoes($umFuncionario);
$controlador->adicionaBonificaoes($umDiretor);
$controlador->adicionaBonificaoes($umEditor);

echo $controlador->recuperaTotalBonificacoes();
